﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;

//If you don't know what happens, me either
//Andrew Machuzhak


namespace toDo_list
{
    public partial class Form1 : Form
    {

        SQLiteConnection sqlite_conn; 
        SQLiteCommand cmd;
        SQLiteDataReader rdr;

        List<string> col;
        
        string source = "tasks.db";

        public Form1()
        {

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) // add task
        {
            string stm = "CREATE TABLE IF NOT EXISTS Tasks (id INTEGER PRIMARY KEY AUTOINCREMENT,task_name TEXT, gruppa TEXT, checked INTEGER)";
            write_to_bd(stm, source);

            string input = textBox1.Text;  //user input
            if (input.Contains(";"))
            {
                string[] split = input.Split(new char[] { ';' }); // one half is task name, other is group

                if (split[0] == "" || split[1] == "")
                {
                    MessageBox.Show("Завдання або категорія не може бути пустим!\n" + "Формат введення: [Завдання];[категорія]", "Повідомлення",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                try
                {
                    stm = "INSERT INTO tasks ('task_name', 'gruppa', 'checked') values ('" + split[0].Trim() + "','" + split[1].Trim() + "','"+ 0 +"')";
                    write_to_bd(stm, source);
                    textBox1.Clear();
                    update_main_view();
                }
                catch
                {
                    MessageBox.Show("Неправильно введено завдання!\n" + "Формат введення: [Завдання];[категорія]", "Повідомлення",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Неправильно введено завдання!\n" + "Формат введення: [Завдання];[категорія]", "Повідомлення",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Form1_Load(object sender, EventArgs e) // when form loads
        {
            
            if (!File.Exists("tasks.db"))
            {
                SQLiteConnection.CreateFile("tasks.db");
            }
            else
            {
                string stm = "CREATE TABLE IF NOT EXISTS Tasks (id INTEGER PRIMARY KEY AUTOINCREMENT, task_name TEXT, gruppa TEXT, checked INTEGER)";
                write_to_bd(stm, source);

                
                stm = "SELECT task_name, gruppa, checked from tasks";
                rdr = read_form_bd(stm, source);
                
                menuStrip1.Items.Add("All");  //group for viewing all tasks
                List<string> col = new List<string> { };
                while (rdr.Read())
                {
                    string temp1 = rdr.GetString(0); // task name
                    string temp2 = rdr.GetString(1); // group(category)
                    int temp3 = rdr.GetInt16(2); // check
                    
                    checkedListBox1.Items.Add(temp1.ToString());
                    checkedListBox1.SetItemChecked(checkedListBox1.Items.Count - 1, Convert.ToBoolean(temp3));

                    col.Add(temp2);
                }

                col = col.Distinct().ToList();  // after adding new items to existing category there are accurences, we need to delete them. 
                foreach (string i in col)
                {
                    menuStrip1.Items.Add(i);
                }
                for (int i = 0; i < menuStrip1.Items.Count; i++)
                {
                    menuStrip1.Items[i].Click += new System.EventHandler(this.change_gruppa);
                }
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string item = checkedListBox1.SelectedItem.ToString();
                string stm = "select gruppa from Tasks where task_name = '" + item + "'";
                rdr = read_form_bd(stm, source);
                string text = "";
                while (rdr.Read())
                {
                    string s = rdr.GetString(0);
                    text = item + ";" + s;
                }
                editForm form2 = new editForm(text);
                form2.Show();
                form2.FormClosed += new FormClosedEventHandler(Inicio_FormClosed_1); ;

            }
            catch
            {
                MessageBox.Show("Оберіть запис для редагування!\n", "Повідомлення",
                  MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }
        private void Inicio_FormClosed_1(object sender, FormClosedEventArgs e)
        {
            update_main_view();
        }
        private void edit(object sender, EventArgs e)
        {
            
            string stm = "UPDATE Tasks SET  task_name = '" + textBox1.Text + "' where task_name = '" + checkedListBox1.SelectedItem.ToString() + "'";
            write_to_bd(stm, source);
        }
        private void change_gruppa(object sender, EventArgs e) // changes checkboxlist view when pressed on category
        {
            
            string stringquery;
            if (sender.ToString() != "All")
            {
                stringquery = "select task_name, checked from tasks where gruppa = \"" + sender.ToString() + "\"";
            }
            else
            {
                stringquery = "select task_name, checked from tasks";
            }

            rdr = read_form_bd(stringquery, source);
            checkedListBox1.Items.Clear();
            while (rdr.Read())
            {
                string temp = rdr.GetString(0);
                bool check = Convert.ToBoolean(rdr.GetInt16(1));
                checkedListBox1.Items.Add(temp);
                checkedListBox1.SetItemChecked(checkedListBox1.Items.Count - 1, check);

            }
        }
        private void update_main_view() // updates all from db
        {
            string stm = "SELECT task_name, gruppa, checked from tasks";
            
            rdr = read_form_bd(stm, source);
            
            col = new List<string> { };
            
            checkedListBox1.Items.Clear();
            while (rdr.Read())
            {
                string temp1 = rdr.GetString(0);
                string temp2 = rdr.GetString(1);
                int temp3 = rdr.GetInt16(2);
                checkedListBox1.Items.Add(temp1);
                checkedListBox1.SetItemChecked(checkedListBox1.Items.Count - 1, Convert.ToBoolean(temp3));
                col.Add(temp2);
            }
            rdr.Close();
            col = col.Distinct().ToList();
            menuStrip1.Items.Clear();
            menuStrip1.Items.Add("All");
            foreach (string i in col)
            {
                menuStrip1.Items.Add(i);
            }
            for (int i = 0; i < menuStrip1.Items.Count; i++)
            {
                menuStrip1.Items[i].Click += new System.EventHandler(this.change_gruppa);
            }
            
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e) //works when checkbox is changed 
        {
            string stm = "";
            
            
            int index = checkedListBox1.SelectedIndex;
            
            try
            {
                if (checkedListBox1.GetItemChecked(index)) //checks whether item is checked or not
                {
                    stm = "UPDATE Tasks SET checked = 1 where task_name = '" + checkedListBox1.Items[index].ToString() + "'";
                }
                else
                {
                    stm = "UPDATE Tasks SET checked = 0 where task_name = '" + checkedListBox1.Items[index].ToString() + "'";
                }
               
                write_to_bd(stm, source);
            }
            catch
            {
                //it's ok
            }
        }
        public void write_to_bd(string stm, string source) // write to db
        {
            sqlite_conn = new SQLiteConnection("Data Source=" + source + ";Version=3;"); 
            sqlite_conn.Open();
            cmd = new SQLiteCommand(sqlite_conn);
            cmd.CommandText = stm;
            cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }
        public SQLiteDataReader read_form_bd(string stm, string source) // read from db
        {
            sqlite_conn = new SQLiteConnection("Data Source=" + source + ";Version=3;");
            sqlite_conn.Open();
            cmd = new SQLiteCommand(stm, sqlite_conn);
            rdr = cmd.ExecuteReader();
            return rdr;
        }

        private void button4_Click(object sender, EventArgs e) // task's deletion
        {
            
            try
            {
                string stm = "DELETE FROM tasks WHERE task_name = '" + checkedListBox1.SelectedItem.ToString() + "'; ";
                write_to_bd(stm, source);
                update_main_view();
            }
            catch
            {
                MessageBox.Show("Щоб видалити запис потрібно його обрати!\n" , "Повідомлення",
                               MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
namespace toDo_list
{
    public partial class editForm : Form
    {
        string edit_Text;
        public editForm(string str)
        {
            InitializeComponent();
            textBox1.Text = str;
            edit_Text = str;
            

        }
        SQLiteConnection sqlite_conn;
        SQLiteCommand cmd;
        SQLiteDataReader rdr;
        string source = "db\\tasks.db";



        public void write_to_bd(string stm, string source) // write to db
        {
            sqlite_conn = new SQLiteConnection("Data Source=" + source + ";Version=3;");
            sqlite_conn.Open();
            cmd = new SQLiteCommand(sqlite_conn);
            cmd.CommandText = stm;
            cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }
        public SQLiteDataReader read_form_bd(string stm, string source) // read from db
        {
            sqlite_conn = new SQLiteConnection("Data Source=" + source + ";Version=3;");
            sqlite_conn.Open();
            cmd = new SQLiteCommand(stm, sqlite_conn);
            rdr = cmd.ExecuteReader();
            return rdr;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string[] old_str = edit_Text.Split(new char[] { ';' });
                string[] new_str = textBox1.Text.Split(new char[] { ';' });
                
                string stm = "update Tasks set 'task_name' = '"+ new_str[0] +"' where task_name ='"+ old_str[0] +"';"+
                    "update Tasks set 'gruppa' = '"+ new_str[1] +"' where task_name = '"+ new_str[0]+"'";
                write_to_bd(stm, source);
                this.Close();
            }
            catch
            {
                MessageBox.Show("Спробуйте ще раз! \n" + "Формат введення: [Завдання];[категорія]", "Повідомлення",
                  MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

    }

}
